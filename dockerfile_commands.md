## Label
LABEL allows you to document your images by adding helpful metadata to them. Labels accept a single argument: two words joined together by an equal sign. This is also called a key-value pair. 
```
LABEL maintainer="Rey Panganiban <reynaldo.panganiban.jr@gmail.com>"
LABEL environment="production"
```

## Workdir
`WORKDIR` allows you to configure a working directory for RUN commands within your Dockerfile file, and/or containers created from your image. I find WORKDIR really useful for executing multiple RUN commands that rely on the same directory. This prevents me from repeating myself when I write my RUN commands.

```
WORKDIR /app
# /app has 2 scripts in it /app/script1.sh & /app/script2.sh
RUN /bin/bash script1.sh
RUN /bin/bash script2.sh
ENTRYPOINT [ "./script1.sh" ]
```

## User
`USER` allows you to change the Linux or Windows user used by RUN commands and/or containers created from your image.
```
USER root
RUN apt -y update && apt -y install curl
RUN useradd -p supersecret newuser

USER newuser
ENTRYPOINT [ "/app/app.sh" ]
```

## Expose
`EXPOSE` command documents network ports that containers created from your image should expose at runtime. `EXPOSE` does not automatically expose ports
```
USER root
RUN apt -y update && apt -y install curl
RUN useradd -p supersecret newuser

USER newuser
ENTRYPOINT [ "/app/app.sh" ]
EXPOSE 8080
```