## Networking within docker
```
# default network driver is bridge
docker network

Usage:  docker network COMMAND

Manage networks

Commands:
  connect     Connect a container to a network
  create      Create a network
  disconnect  Disconnect a container from a network
  inspect     Display detailed information on one or more networks
  ls          List networks
  prune       Remove all unused networks
  rm          Remove one or more networks

docker network create network-a
docker network create network-b

docker network inspect network-a

docker container create --user root -it --name container-a --entrypoint sh --net network-a curlimages/curl
docker container create --user root -it --name container-b --entrypoint sh --net network-b curlimages/curl

docker container start container-a
docker container attach container-a

# attach the container-a to network-b so it can communicate to container-b
docker container stop container-a
docker network connect network-b container-a
```

## Exposing container ports between containers
```
# in this scenario, container-a is network-a and container-b is on network-b
# which means both of the containers wont be able to talk to each other

# recreate the containers publishing port 80 --> 8080
# mapping port 8080 on the host machine to port 80 of the container [OUTSIDE:INSIDE]
docker container create --user root -it --name container-a --entrypoint sh --net network-a --publish 8080:80 curlimages/curl
docker container create --user root -it --name container-b --entrypoint sh --net network-b --publish 8081:80 curlimages/curl
```

## host mode networking
```
# configures containers to use their host's network interface and network stack
# this uses the host's IP address
docker run --rm --net=host -i --entrypoint nc curlimages/curl -l -p 8080

# so to access the netcat on the container above, you can use localhost
```

## Advanced network drivers and network
```
# `macvlan` and `ipvlan` give your containers real IP addresses on your network.
# `macvlan` networks give containers individual MAC addresses which can lead to a network degradation
# `docker network create -d macvlan --subnet SUBNET -o INTERFACE.VLAN_TAG --gateway GATEWAY NETWORK_NAME`
#     eth0 is the network interface card of the host where you want to bind this conatiner network
docker network create -d macvlan --subnet 192.168.1.0/24 -o eth0.VLAN1 --gateway 192.168.1.1 my-net

# --aux-address excludes the IP addresses
docker network create -d macvlan \
  --subnet 192.168.1.0/24 \
  -o eth0.VLAN1 --gateway 192.168.1.1 \
  --aux-address "host=192.168.1.2" \
  --aux-address "host=192.168.1.3" \
  my-net
# --ip-range allocates IP addresses from the specified ip range
docker network create -d macvlan \
  --subnet 192.168.1.0/24 \
  -o eth0.VLAN1 --gateway 192.168.1.1 \
  --ip-range 192.168.1.64/26 \
  my-net

# `ipvlan` gives containers a shared MAC address

# `overlay` network driver. This driver allows you to create a virtual network across two or more nodes running the Docker engine, 
# or a Swarm. Swarms are provided by Docker Swarm, Swarm's official container orchestrator. Swarm allows you to create and manage 
# containers across multiple machines.
```