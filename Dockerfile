FROM ubuntu
COPY . /app
RUN chmod +x /app/app.sh

# `ARG curl_bin="curl"`
# build arguments are variables you provide to docker build
# to build docker image with ARG, you must use --build-arg $VARIABLE="<VALUE>"
#   `docker build --build-arg curl_bin="curl-7.61.1-22" -t my-image -f Dockerfile .`

ENV curl_bin="curl-7.61.1-22"
# ENV configures environment variables for containers started from this image

# ENV variables are defined for every container started from an image. 
# Whereas ARG variables are only used during runtime. This makes ENV variables great 
# for providing default configurations for apps while still allowing users to change them 
# when they start containers with docker run.
# ARGs are variables that are set at build time whereas ENVs are set at run time

RUN apt -y update && \
    apt -y install "$curl_bin"


# to build this docker:
#   `docker build -t image_tag -f Dockerfile .
# to run the container in a non-interactive mode
#   --rm -> removes the container once it was ran
# `docker run --rm nginx_ubuntu:start_service /usr/sbin/service nginx start && curl localhost`

# ENTRYPOINT configures containers to run stuff when a container is created from it

ENTRYPOINT[ "/app/app.sh"]