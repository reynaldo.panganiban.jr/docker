## Container storage - volume
https://docs.docker.com/engine/extend/legacy_plugins/#volume-plugins
  volume drivers define how volumes are created and managed by containers. By default, Docker uses the local volume driver. The local driver simply creates a directory within Docker system directory, and mounts it into the container like a virtual disk of sorts.

## Creating and using volumes
- Use the `docker volume create test-vol` command to create a volume
```
docker inspect test-vol
[
    {
        "CreatedAt": "2024-06-03T20:05:05+08:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/test-vol/_data",
        "Name": "test-vol",
        "Options": null,
        "Scope": "local"
    }
]
```
- You can change drivers with the `-d` option
Your volume driver might have additional configuration options. If so, use the -o flag to specify them. Linking a volume to a container is similarly straightforward, but with a slight twist. There are two ways to mount a volume to your container. The short way with `--volume`, or `-v` for short, or the long way with `--mount`.

The long way `docker run --mount "type=TYPE,source=DIR,destination=dir,readonly,volume-opt"`
- `type`(mandatory): specifies the type of volume we are creating(not the driver) - `bind`, `tmpfs`, and `volume`
- `source`(mandatory): the name of the volume created during `docker volume create`
- `destination`(mandatory): the folder within the container to mount to this volume
- `readonly`: marks a volume as read only
- `volume-opt`: options to provide the volume driver with

The short way `docker run -v "source:destination:[ro]"`

```
docker volume create docker-volume

# the long way
docker run -it --rm --mount 'type=volume,source=docker-volume,destination=/docker-volume' alpine

docker volume inspect docker-volume
```

## The bind mount
Bind mounts accept two advanced configuration parameters: `bind-propagation` and `selinux-label`. `bind-propagation` tells Linux how mounts within the mount or sub-mounts are handled internally by Linux. `selinux-label` specifies whether the directory being mounted can be shared with other containers. It must be colon lowercase z for mounts that will be shared with other containers or colon capital Z for mounts that will not.

The long way `docker run --mount "type=bind,source=docker-volume,destination=/docker-volume,readonly,[advanced]"`
The short way `docker run -v "source:destination:[ro]"`
- When you're using the short way on some systems, the source directory might need to be an absolute directory.
- If you try to mount a source directory that does not exist using the short way, Docker will automatically create a directory for you on your computer.

```
mkdir -p /tmp/test/stuff
for i in {1..10}; do echo "This file number ${i}" > /tmp/test/stuff/file-$i; done

# mounting this directory to a container
docker run --rm -v /tmp/test/stuff:/stuff alpine
```

## copying data from volumes
```
# create a backup
docker volume create test-vol
docker run --rm -v $PWD:/target -v test-vol:/backup alpine tar cvzf /target/backup.tar -C /backup .
tar -tf backup.tar

# restore a backup (copying)
docker volume create test-vol-restore
docker run --rm -v $PWD:/target -v test-vol-restore:/restore alpine tar xvf /target/backup.tar -C /restore .
```