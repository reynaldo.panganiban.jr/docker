## Container image registries
```
docker run --rm --name registry -p 5000:5000 registry:2

docker tag my-image:latest localhost:5000/my-image:latest
docker push localhost:5000/my-image:latest

docker run --rm localhost:5000/my-image
```

```
docker tag my-image:latest-arm localhost:5000/my-image:latest-arm
docker tag my-image:latest localhost:5000/my-image:latest

docker push localhost:5000/my-image:latest-arm
docker push localhost:5000/my-image:latest

docker manifest create --insecure localhost:5000/my-image --amend localhost:5000/my-image:latest --amend localhost:5000/my-image-arm
docker manifest inspect localhost:5000/my-image

docker images --digests localhost:5000/my-image
docker manifest push localhost:5000/my-image
```

## Creating and logging into authenticated registries
```
# create a network called registry
docker network create registry
./create_unsigned_certs.sh
sudo update-ca-certificates
docker run --entrypoint htpasswd httpd:2 -Bbn admin supersecretpassword > ./htpasswd
docker run --rm --name registry-backend --network registry -d registry:2

# create a container registry with the official nginx image
docker run --rm -v $PWD/nginx.conf:/etc/nginx/nginx.conf -v $PWD/htpasswd:/auth/htpasswd -v $PWD/certs:/certs --network registry -p 5000:5000 -d nginx

# login to the container registry
docker login localhost:5000
# username=admin
# password=supersecretpassword

docker push localhost:5000/my-image
docker run --rm localhost:5000/my-image
```

create_unsigned_certs.sh
```
#!/usr/bin/env bash
EXERCISE_FILES_DIR="$(dirname "$0")"
OPENSSL_COMMAND=(
  openssl                                 # OpenSSL is a program that manages SSL certificates. Very useful to learn.
  req                                     # Tells OpenSSL to request a new cert. Since we are self-signing it, this request will be granted immediately.
  -x509                                   # -x509 tells openssl that this is a self-signed certificate.
  -newkey rsa:4096                        # This tells OpenSSL to create a private key for the cert with a 4096-bit long RSA key.
  -keyout /work/certs/key.pem             # This exports our RSA key to our certs directory.
  -out /work/certs/cert.pem               # This exports our cert to our certs directory.
  -nodes                                  # This prevents OpenSSL from encrypting the private key generated earlier.
  -sha256                                 # This uses a SHA256 hash to sign the cert.
  -subj "/CN=localhost"                   # This configures the certificate to be associated with our own computer.
)

# Remove the certs directory, if it exists.
test -d "${EXERCISE_FILES_DIR}/certs" && rm -rf "${EXERCISE_FILES_DIR}/certs"
mkdir -p "${EXERCISE_FILES_DIR}/certs"

# Create an Alpine container with a /work directory bind-mounted to our exercise files directory.
# Use it to install OpenSSL via "apk add openssl" and run our OpenSSL command above.
docker run --rm -v "$EXERCISE_FILES_DIR:/work" -w /work alpine \
  sh -c "apk add openssl && ${OPENSSL_COMMAND[*]}"
```

nginx.conf
```
events {
    worker_connections  1024;
}

http {

  # This informs NGINX about an external web server, or an
  # "upstream" server, that can be used in other parts of this configuration.
  upstream docker-registry {
    server registry-backend:5000;
  }

  ## Set a variable to help us decide if we need to add the
  ## 'Docker-Distribution-Api-Version' header.
  ## The registry always sets this header.
  ## In the case of nginx performing auth, the header is unset
  ## since nginx is auth-ing before proxying.
  map $upstream_http_docker_distribution_api_version $docker_distribution_api_version {
    '' 'registry/2.0';
  }

  server {
    # This is the port that our reverse proxy is listening on.
    listen 5000 ssl;

    # This is the host name of our server, i.e., the thing that you type into your address bar or curl.
    server_name registry;

    error_log /tmp/nginx.log debug;

    # This header enforces HTTP Strict Transport Security, or HSTS.
    # HSTS tells browsers that an endpoint should send all insecure HTTP traffic to its
    # HTTPS port.
    add_header Strict-Transport-Security "max-age=31536000" always;

    # This uses NGINX's internal http code 497 to temporarily redirect requests to another URL.
    # (HTTP 301 is the code for temporary redirects.)
    # Here, we're simply sending the HTTP request to our HTTPS listener on line 24.
    error_page 497 =301 https://$host:$server_port$request_uri;

    # These are the CA cert and key that we'll present during HTTPS connections!
    ssl_certificate /certs/cert.pem;
    ssl_certificate_key /certs/key.pem;

    # Recommendations from https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html
    ssl_protocols TLSv1.1 TLSv1.2;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;

    # disable any limits to avoid HTTP 413 for large image uploads
    client_max_body_size 0;

    # required to avoid HTTP 411: see Issue #1486 (https://github.com/moby/moby/issues/1486)
    chunked_transfer_encoding on;

    # This adds the mandatory /v2 endpoint used by the Docker registry in the backend.
    location /v2/ {
      # This enables basic authentication. Other authentication modules exist for NGINX as well.
      auth_basic "Registry realm";
      auth_basic_user_file /auth/htpasswd;

      ## If $docker_distribution_api_version is empty, the header is not added.
      ## See the map directive above where this variable is defined.
      add_header 'Docker-Distribution-Api-Version' $docker_distribution_api_version always;

      # This block tells NGINX to send traffic to https://localhost:5000/v2 to our backend registry.
      proxy_pass                          http://docker-registry;
      proxy_set_header  Host              $http_host;   # required for docker client's sake
      proxy_set_header  X-Real-IP         $remote_addr; # pass on real client's IP
      proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
      proxy_set_header  X-Forwarded-Proto $scheme;
      proxy_read_timeout                  900;
    }
  }
}

```