## Docker in docker [dind]
There are several reasons why you might find yourself in this situation. Let's say that you're writing tests for your app, but want your test suite to behave the same way on any machine like GitHub Actions Runners. Since the operating system and hardware of the build servers running your test will not be consistent, having your test run as containers that then start your app also as a container is useful. Or let's say that you need to write an application that needs to create or manage containers.
Instead of attempting to run the Docker engine within Docker, we're going to configure our container to communicate with our existing Docker engine and create containers from it. We'll do this by bind mounting the UNIX socket used by the Docker engine to the container.

```
# `/var/run/docker.sock:/var/run/docker.sock` is called bind mounting
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -it my-image

docker volume create temp_vol
docker container create -v temp_vol:/tmp alpine -t my-image
docker cp /app my-image:/tmp/app

docker run --rm -it -v temp_vol:/tmp my-image
```