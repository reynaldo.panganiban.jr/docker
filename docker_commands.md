## docker commands - extending container priviliges with capabilities [CAP]
# https://linux.die.net/man/7/capabilities
`docker run --entrypoint sh --rm alpine -c 'touch /tmp/file && chown 1001:1001 /tmp/file && echo "File permission changed."'`

```
docker run --entrypoint sh --cap-drop CAP_CHOWN --rm alpine -c 'touch /tmp/file && chown 1001:1001 /tmp/file && echo "File permission changed."'
# chown: /tmp/file: Operation not permitted

docker run --entrypoint sh --cap-drop ALL --cap-add CAP_CHOWN --rm alpine -c 'touch /tmp/file && chown 1001:1001 /tmp/file && echo "File permission changed."'
```

## setting container limits
```
docker run --rm --cpus 0.8 stress-container --cpu 2 --timeout 5

docker run --rm --memory 2G --cpus 0.8 stress-container --vm 1 --vm-bytes 1.99G --timeout 15
```

## configuring logging with logging drivers
```
docker run --name test-container --log-driver none test-app # disable docker logs
# Hello, rey! Last step: run this app again, but put '--finish' at the end of your 'docker run' command

docker logs test-container
# Error response from daemon: configured logging driver does not support reading

docker inspect test-container --format '{{.ID}}' # to get the ID of the container
```

## configuring the docker daemon
```
dockerd --help
```